"""
Module for splitting given video by timestamps or by seconds
from information given in corresponding `.csv` file.

Also is used for extracting frames from given video for further analysis.
"""

import os
from io import StringIO

import cv2
import pandas as pd

from moviepy.video.io.VideoFileClip import VideoFileClip


class VideoSplitter:
    """
    Class for splitting video based on input timestamps.
    """
    def __init__(self, video_folder, video_filename, scenes_directory):
        self.video_filename = video_filename
        self.scenes_directory = scenes_directory
        self.path_to_video = os.path.join(video_folder, self.video_filename)
        self.video_name = self.video_filename.split('.')[0]

        # Input video parameters
        self.clip = VideoFileClip(self.path_to_video)
        self.fps = self.clip.fps
        self.size = self.clip.size
        self.duration = self.clip.duration
        self.num_frames = int(self.fps * self.duration)

        # self.clip.reader.close()
        # self.clip.audio.reader.close_proc()

    def split_by_seconds(self):
        """
        Method to split video by seconds and save results.
        """
        path_to_scenes = os.path.join(self.video_name, 'scenes')
        if not os.path.exists(path_to_scenes):
            os.makedirs(path_to_scenes)
        with open(self.scenes_directory) as file:
            scenes_data = pd.read_csv(StringIO(file.read()), skiprows=1)
            for i in range(len(scenes_data['Start Time (seconds)'])):
                scenes_data['Start Time (seconds)'][i] += (1.000 / self.fps)
                scenes_data['End Time (seconds)'][i] -= (1.000 / self.fps)

                shot = self.clip.subclip(scenes_data['Start Time (seconds)'][i],
                                         scenes_data['End Time (seconds)'][i])

                # default codec: 'libx264', 24 fps
                shot.write_videofile(filename='{0}/{1}_scene_{2}.mp4'
                                     .format(path_to_scenes, self.video_name, i + 1),
                                     fps=self.fps)
                # shot.close()

    def split_by_timestamps(self):
        """
        Method to split video by timestamps and save results.
        """
        # print(self.scenes_directory)
        with open(self.scenes_directory) as file:
            timestamps = file.readline().strip('\n').split(',')[1:]
            timestamps.insert(0, '00:00:00.000')

        for i in range(len(timestamps) - 1):
            # print(timestamps[i], timestamps[i + 1])
            # print(timestamps[i])
            # print(timestamps[i + 1])
            shot = self.clip.subclip(timestamps[i], timestamps[i + 1])
            # print('{0}/{0}_scene_{1}.mp4'.format(self.video_name, i + 1))
            # print(self.fps)
            # default codec: 'libx264', 24 fps
            shot.write_videofile(filename='{0}/{0}_scene_{1}.mp4'
                                 .format(self.video_name, i + 1),
                                 fps=self.fps)

    def extract_all_frames(self):
        """
        Method for extracting all frames as .jpg images from input video
        and saving them in corresponding directory based on video name.
        """
        vidcap = cv2.VideoCapture(self.path_to_video)
        success, image = vidcap.read()
        count = 0

        path_to_frames = os.path.join(self.video_name, 'frames')
        if not os.path.exists(path_to_frames):
            os.makedirs(path_to_frames)

        while success:
            # Saves frames as JPEG files
            cv2.imwrite(os.path.join(path_to_frames, '{}_frame_{}.jpg'.format(self.video_name, count + 1)), image)
            success, image = vidcap.read()
            print('Read a new frame: ', success)
            count += 1

    def extract_frames(self, path_to_video):
        """
        Method for extracting frames as .jpg images from input video (scene)
        and saving them in corresponding directory based on video name.
        """
        scene_number = path_to_video.split('\\')[-1].split('.')[0].split('_')[-1]

        vidcap = cv2.VideoCapture(path_to_video)
        success, image = vidcap.read()
        count = 0

        path_to_frames = os.path.join(self.video_name, 'frames')
        if not os.path.exists(path_to_frames):
            os.makedirs(path_to_frames)

        while success:
            # Saves frames as JPEG files
            cv2.imwrite(os.path.join(path_to_frames, "Scene_{}_frame{}.jpg"
                                     .format(scene_number, count)), image)
            success, image = vidcap.read()
            print('Read a new frame: ', success)
            count += 1

    def extract_frames_by_scene(self):
        """
        Extract all frames for each scene `.mp4` video in folder.
        """
        for video_path in os.listdir(self.video_name):
            if video_path.endswith(".mp4"):
                self.extract_frames(os.path.join(self.video_name, video_path))


if __name__ == '__main__':
    PATH_TO_ALL_VIDEOS = 'test_videos'
    VIDEO_FILENAME = 'Zero.mp4'
    PATH_TO_SCENES = 'Zero/Zero-Scenes.csv'

    splitter = VideoSplitter(PATH_TO_ALL_VIDEOS, VIDEO_FILENAME, PATH_TO_SCENES)

    # splitter.split_by_timestamps()
    splitter.split_by_seconds()

    # splitter.extract_all_frames()

    # splitter.extract_frames('C:\\Users\\orest.rehusevych\\PycharmProjects\\movie2trailer\\PySceneDetect\\LOTR'
    #                         '\\LOTR_scene_99.mp4')
    # splitter.extract_frames('C:\\Users\\orest.rehusevych\\PycharmProjects\\movie2trailer\\PySceneDetect\\LOTR'
    #                         '\\LOTR_scene_23.mp4')
    # splitter.extract_frames_by_scene()
    # splitter.split_by_timestamps()

    # split_into_shots_moviepy(PATH_TO_VIDEO, PATH_TO_SCENES)
    # split_into_shots_ffmpeg(PATH_TO_VIDEO, PATH_TO_SCENES)
    # split_into_shots(PATH_TO_VIDEO, PATH_TO_SCENES)

