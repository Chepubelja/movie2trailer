from __future__ import print_function
import os

import pandas as pd

# Standard PySceneDetect imports:
from scenedetect.video_manager import VideoManager
from scenedetect.scene_manager import SceneManager

# For caching detection metrics and saving/loading to a stats file
from scenedetect.stats_manager import StatsManager

# For content-aware scene detection:
from scenedetect.detectors.content_detector import ContentDetector


def find_scenes(video_path, threshold=30, min_frames=15):
    """# type: (str) -> List[Tuple[FrameTimecode, FrameTimecode]]"""
    video_manager = VideoManager([video_path])
    stats_manager = StatsManager()

    # Construct our SceneManager and pass it our StatsManager.
    scene_manager = SceneManager(stats_manager)

    # Add ContentDetector algorithm (each detector's constructor
    # takes detector options, e.g. threshold).
    scene_manager.add_detector(ContentDetector(threshold=threshold, min_scene_len=min_frames))
    base_timecode = video_manager.get_base_timecode()

    # We save our stats file to {VIDEO_PATH}.stats.csv.
    stats_file_path = '%s.stats.csv' % video_path.split('.')[0]

    try:
        # If stats file exists, load it.
        if os.path.exists(stats_file_path):
            # Read stats from CSV file opened in read mode:
            with open(stats_file_path, 'r') as stats_file:
                stats_manager.load_from_csv(stats_file, base_timecode)

        # Set downscale factor to improve processing speed.
        video_manager.set_downscale_factor()

        # Start video_manager.
        video_manager.start()

        # Perform scene detection on video_manager.
        scene_manager.detect_scenes(frame_source=video_manager)

        # Obtain list of detected scenes.
        scene_list = scene_manager.get_scene_list(base_timecode)
        # Each scene is a tuple of (start, end) FrameTimecodes.

        print('List of scenes obtained:')
        print(len(scene_list))

        detected_scenes = pd.DataFrame()

        for i, scene in enumerate(scene_list):
            print(
                'Scene Number %2d: '
                'Start Frame %d, '
                'Start Timecode %s '
                'Start Time (seconds) %s / '
                'End Frame %d '
                'End Timecode %s '
                'End Time (seconds) %s / '
                'Seconds %s / ' % (
                    i + 1,
                    scene[0].get_frames(),
                    scene[0].get_timecode(),
                    scene[0].get_seconds(),
                    scene[1].get_frames(),
                    scene[1].get_timecode(),
                    scene[1].get_seconds()
                ))

        # We only write to the stats file if a save is required:
        # if stats_manager.is_save_required():
        #     with open(stats_file_path, 'w') as stats_file:
        #         stats_manager.save_to_csv(stats_file, base_timecode)

    finally:
        video_manager.release()

    return scene_list


THRESHOLDS = [20, 25, 30]

# for thresh in THRESHOLDS:
#     find_scenes('test_videos/LOTR_HD.mp4', threshold=thresh, min_frames=15)

find_scenes('test_videos/LOTR.mp4', threshold=30, min_frames=15)
