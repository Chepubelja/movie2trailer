import subprocess
import pandas as pd

from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
from PySceneDetect.split_video import VideoSplitter


def split_into_shots_ffmpeg(path_to_video, path_to_scenes):
    video_name = path_to_video.split('/')[1].split('.')[0]

    with open(path_to_scenes) as file:
        timestamps = file.readline().strip('\n').split(',')[1:]
        timestamps.insert(0, '00:00:00.000')

    for i in range(len(timestamps) - 1):
        cmd = ["ffmpeg", "-i", path_to_video,
               "-ss", timestamps[i], "-to", timestamps[i + 1],
               "-c", "copy",
               "-framerate", '{}'.format(VideoSplitter.get_video_fps(path_to_video)),
               '{0}/{0}_scene_{1}.mp4'.format(video_name, i + 1)]
        subprocess.run(cmd, stderr=subprocess.STDOUT)


def split_into_shots_moviepy(path_to_video, path_to_scenes):
    video_name = path_to_video.split('/')[1].split('.')[0]

    timestamps = pd.read_csv(path_to_scenes).iloc[:, [6]].values.tolist()[1:]
    timestamps.insert(0, ['0.000'])
    timestamps = [float(timestamp[0]) for timestamp in timestamps]

    for i in range(len(timestamps) - 1):
        ffmpeg_extract_subclip(filename=path_to_video,
                               t1=timestamps[i],
                               t2=timestamps[i + 1],
                               targetname='{0}/{0}_scene_{1}.mp4'.format(video_name, i + 1))
