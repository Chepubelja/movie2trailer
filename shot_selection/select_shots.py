"""
Module for selecting shots for final trailer creation
based on previously detected anomalous/outlier frames.
"""

import os

import numpy as np
import pandas as pd
from moviepy.editor import VideoFileClip, concatenate_videoclips


def construct_selected_shots_video(scenes_numbers: list, path_to_scenes_videos: str, result_video_name: str):
    """
    Constructs video from given list with scenes numbers.
    :param scenes_numbers: List with scene numbers.
    :param path_to_scenes_videos: Path to scenes videos.
    :param result_video_name: Path to result constructed video.
    """
    selected_videoclips = [VideoFileClip(os.path.join(path_to_scenes_videos, scene_number))
                           for scene_number in scenes_numbers]
    final_clip = concatenate_videoclips(selected_videoclips)
    final_clip.write_videofile(result_video_name)


def get_anomalous_scenes():
    pass


def main(outliers_filename, scenes_filename, scenes_videos_directory):
    outliers = pd.read_csv(outliers_filename)
    scenes = pd.read_csv(scenes_filename, skiprows=1)

    anomaly_scores = np.array([sum(row) for index, row in outliers.iterrows()])

    for threshold in [0, 1, 2, 3, 4, 5]:
        print(threshold)
        print(len(anomaly_scores[anomaly_scores == threshold]))
        print()
    # for outlier_detector in outliers.columns:
    #     print(outlier_detector)


if __name__ == '__main__':
    PATH_TO_OUTLIERS = 'selected_outliers.csv'
    PATH_TO_SCENES = '../movies/Requiem/Requiem-Scenes.csv'
    PATH_TO_SCENES_VIDEOS = '../PySceneDetect/Requiem/scenes'

    main(PATH_TO_OUTLIERS, PATH_TO_SCENES, PATH_TO_SCENES_VIDEOS)
