# -*- coding: utf-8 -*-

import os
import re
import json
from time import time

import numpy as np
import pandas as pd
from luminoth import Detector, read_image, vis_objects


def check_file_existance(filename):
    if os.path.exists(filename):
        file_exists = True
    else:
        file_exists = False
    return file_exists


def run_batch(images_batch, detector):
    # Returns a dictionary with the detections.

    predicted_batch = detector.predict(images_batch)

    return predicted_batch


def save_batch_results(result_filename, images_batch, batch_frame_numbers, file_existence):
    # if file_existence:
    #     result_df = pd.read_csv(result_filename)
    # else:
    #     result_df = pd.DataFrame()

    result_df = pd.DataFrame()

    for frame_i, frame_obj in enumerate(images_batch):
        df = pd.DataFrame.from_dict(frame_obj)
        df.insert(loc=0, column='frame_num', value=[batch_frame_numbers[frame_i] for _ in range(len(df))])

        df['frame_num'] = [batch_frame_numbers[frame_i] for _ in range(len(df))]

        result_df = pd.concat([result_df, df], ignore_index=True, sort=False)

    print(file_existence)
    if not file_existence:
        result_df.to_csv(result_filename, index=False)
    else:
        result_df.to_csv(result_filename, mode='a', header=False, index=False)
    # if file_existence:
    #     result_df.to_csv(result_filename, mode='a', header=False, index=False)
    #     # with open(result_filename, 'a') as file:
    #     #     result_df.to_csv(file, header=False, index=False)
    # # else:
    # result_df.to_csv(result_filename, index=False)

    # try:
    #     result_df = pd.read_csv('test.csv')
    #     for frame_i, frame_obj in enumerate(all_objects):
    #         if frame_i in result_df['frame_num']:
    #             pass
    #         else:
    #             df = pd.DataFrame.from_dict(frame_obj)
    #             # df.insert(loc=0, column='frame_num', value=[frame_i for _ in range(len(df))])
    #             df['frame_num'] = [all_names[frame_i].split('/')[-1].split('.')[0].split('_')[-1]
    #                                for _ in range(len(df))]
    #             result_df = pd.concat([result_df, df], ignore_index=True, sort=False)
    #
    #             if frame_i % 10 == 0:
    #                 print('Saved -  {}/{}'.format(frame_i, len(all_objects)))
    #                 with open('test.csv', 'a') as f:
    #                     result_df.to_csv(f, header=False, index=False)
    # except FileNotFoundError:
    #     result_df = pd.DataFrame()
    #     for frame_i, frame_obj in enumerate(all_objects):
    #         df = pd.DataFrame.from_dict(frame_obj)
    #         # df.insert(loc=0, column='frame_num', value=[frame_i for _ in range(len(df))])
    #         df['frame_num'] = [all_names[frame_i].split('/')[-1].split('.')[0].split('_')[-1] for _ in range(len(df))]
    #         result_df = pd.concat([result_df, df], ignore_index=True, sort=False)
    #
    #         # if frame_i % 10 == 0:
    #         #     with open('test.csv', 'a') as f:
    #         #         result_df.to_csv(f, header=False, index=False)
    #     result_df.to_csv('test.csv', index=False)


def run_lumi_predict(path_to_frames, result_filename, fast=False, batch_size=1000):
    """
    Function for making object detection prediction on the given image
    and saving results in the right format.
    :param path_to_frames: Path to the folder with all frames.\
    :param result_filename: Name of the file to write results into.
    :param fast: If fast - use SSD and Pascal VOC labels (20 classes),
            else - use Faster R-CNN with MS COCO labels (80 classes).
    :param batch_size: Size of the images batch.
    """
    all_names = [os.path.join(path_to_frames, frame_name)
                 for frame_name in sorted(os.listdir(path_to_frames))]
    # print(all_names)
    print(len(all_names))
    # print(all_names)
    # all_names_string = ' '.join(all_names)
    # print(all_names_string)
    start_time_all = time()

    # print(all_images)

    # If no checkpoint specified, will assume `accurate` by default. In this case,
    # we want to use our traffic checkpoint. The Detector can also take a config
    # object.
    if fast:
        detector = Detector(checkpoint='fast')
    else:
        detector = Detector(checkpoint='accurate')

    all_times = []

    for batch_index in range(0, len(all_names), batch_size):
        start_time = time()
        print(batch_index, batch_index + batch_size)

        batch_frame_numbers = [re.split('[_ .]', frame_name)[-2] for frame_name
                               in all_names[batch_index: batch_index + batch_size]]
        # print(batch_frame_numbers)

        images_batch = [read_image(image_name) for image_name
                        in all_names[batch_index: batch_index + batch_size]]
        # print(len(images_batch))
        predicted_batch = run_batch(images_batch, detector)
        file_exists = check_file_existance(result_filename)

        save_batch_results(result_filename, predicted_batch, batch_frame_numbers, file_existence=file_exists)

        all_times.append(time() - start_time)

        print('{}/{}'.format(batch_index, len(all_names)))
        print('Batch time: {} sec'.format(round(time() - start_time, 2)))
        print('Time: {} min - Time to wait: {} min'
              .format(round(sum(all_times) / 60, 2),
                      round((np.mean(all_times) * (len(all_names) - batch_index * batch_size) / 60), 2)))
        print('=============================================================')

    # result_df.to_csv('test.csv', index=False)
    # print(objects)

    # vis_objects(image, objects).save('traffic-out.png')

    # if fast:
    #     result = subprocess.check_output(['lumi', 'predict', '--checkpoint',
    #                                       'fast', '{}'.format(all_names_string)])
    # else:
    #     result = subprocess.check_output(['lumi', 'predict', '--checkpoint',
    #                                       'accurate', all_names_string])
    #
    print('Total time: {} sec'.format(round(time() - start_time_all, 2)))
    # print()

    # print(result)
    # result = json.loads(result.decode().split('\n')[-2])
    # print(result)
    # print()

    # print(result['file'])
    # print(result['objects'])


if __name__ == '__main__':
    PATH_TO_FRAMES = '/home/orestr/movie2trailer/PySceneDetect/Requiem/frames'
    PATH_TO_RESULT_CSV = 'lumi_results.csv'
    run_lumi_predict(PATH_TO_FRAMES, PATH_TO_RESULT_CSV)
