"""
Module for generating features
based on Luminoth (Object Detection) module.
"""

from time import time

import warnings

import numpy as np
import pandas as pd
from pandas.core.common import SettingWithCopyWarning

warnings.filterwarnings('ignore', category=SettingWithCopyWarning)


def get_num_people(data: pd.DataFrame) -> list:
    """
    Retrieves number of people for each frame.
    :param data: Pandas DataFrame with frame number, label, bbox and prob.
    :return: List with number of people on each frame.
    """
    # start_time = time()
    only_with_person = data[data['label'] == 'person']
    temp_res = pd.DataFrame({'num_people': only_with_person.groupby(["frame_num"]).size()}).reset_index()

    temp_frame_num = temp_res['frame_num'].values.tolist()
    unique_frame_nums = np.unique(data['frame_num'])

    num_people = [0 if frame_num not in temp_frame_num
                  else temp_res.loc[temp_res['frame_num'] == frame_num]['num_people'].values[0]
                  for frame_num in unique_frame_nums]

    # print(time() - start_time)
    return num_people


def get_num_non_people(data: pd.DataFrame) -> list:
    """
    Retrieves number of non people objects for each frame.
    :param data: Pandas DataFrame with frame number, label, bbox and prob.
    :return: List with number of non people objects on each frame.
    """
    # start_time = time()
    only_without_person = data[data['label'] != 'person']
    temp_res = pd.DataFrame({'num_non_people': only_without_person.groupby(["frame_num"]).size()}).reset_index()

    temp_frame_num = temp_res['frame_num'].values.tolist()
    unique_frame_nums = np.unique(data['frame_num'])

    num_non_people = [0 if frame_num not in temp_frame_num
                      else temp_res.loc[temp_res['frame_num'] == frame_num]['num_non_people'].values[0]
                      for frame_num in unique_frame_nums]

    # print(time() - start_time)
    return num_non_people


def get_num_all_objects(people: list, non_people: list) -> list:
    """
    Calculates total number of objects on frame.
    :param people: Number of total people number on frame.
    :param non_people: Number of total non-people number on frame.
    :return: List with total number of objects for each frame.
    """
    # start_time = time()

    all_objects = [person + non_person for person, non_person in zip(people, non_people)]

    # print(time() - start_time)
    return all_objects


def calculate_area(bbox):
    """
    Calculates area (in pixels) of detected bbox.
    :param bbox: Bbox with (x_min, y_min, x_max, y_max)
    :return: Calculated area.
    """
    x_min, y_min, x_max, y_max = bbox
    area = (int(x_max) - int(x_min)) * (int(y_max) - int(y_min))
    return area


def get_total_people_area(data: pd.DataFrame) -> list:
    """
    Retrieves total people area (in pixels) for each frame.
    :param data: Pandas DataFrame with frame number, label, bbox and prob.
    :return: List with total people area (in pixels) on each frame.
    """
    # start_time = time()

    # Filtering only data with frames, where person was detected
    only_with_person = data[data['label'] == 'person']
    only_with_person['bbox'] = only_with_person['bbox'].apply(lambda x: eval(x))

    # All frames are grouped by frame numbers
    data_grouped_by_frame_num = only_with_person.groupby('frame_num')

    # All unique frames numbers with person
    unique_frames_with_person = np.unique(only_with_person['frame_num'])

    total_people_area = []

    # Iterating over frame groups
    for frame_num in np.unique(data['frame_num']):
        if frame_num not in unique_frames_with_person:
            total_people_area.append(0)
        else:
            frame_group = data_grouped_by_frame_num.get_group(frame_num)

            # Calculating summary area for all persons on single frame
            area_summed = sum([calculate_area(bbox=frame.bbox)
                               for frame in frame_group.itertuples()])
            total_people_area.append(area_summed)

    # print(time() - start_time)
    return total_people_area


def get_total_non_people_area(data: pd.DataFrame) -> list:
    """
    Retrieves total non-people area (in pixels) for each frame.
    :param data: Pandas DataFrame with frame number, label, bbox and prob.
    :return: List with total non-people area (in pixels) on each frame.
    """
    # start_time = time()

    # Filtering only data with frames, where person wasn't detected
    only_without_person = data[data['label'] != 'person']
    only_without_person['bbox'] = only_without_person['bbox'].apply(lambda x: eval(x))

    # All frames are grouped by frame numbers
    data_grouped_by_frame_num = only_without_person.groupby('frame_num')

    # All unique frames numbers without person
    unique_frames_without_person = np.unique(only_without_person['frame_num'])

    total_non_people_area = []

    # Iterating over frame groups
    for frame_num in np.unique(data['frame_num']):
        if frame_num not in unique_frames_without_person:
            total_non_people_area.append(0)
        else:
            frame_group = data_grouped_by_frame_num.get_group(frame_num)

            # Calculating summary area for all persons on single frame
            area_summed = sum([calculate_area(bbox=frame.bbox)
                               for frame in frame_group.itertuples()])
            total_non_people_area.append(area_summed)

    # print(time() - start_time)
    return total_non_people_area


def get_total_detected_area(people_area: list, non_people_area: list) -> list:
    """
    Calculates total area of detected objects on frame.
    :param people_area: Total people area on frame.
    :param non_people_area: Total non-people area on frame.
    :return: List with total area of objects for each frame.
    """
    # start_time = time()

    total_detected_area = [person_area + non_person_area
                           for person_area, non_person_area in zip(people_area, non_people_area)]

    # print(time() - start_time)
    return total_detected_area


def process_chunk(chunk: pd.DataFrame, column_names: list, num_frames: int) -> pd.DataFrame:
    """
    Function for generating all features for single chunk.
    :param chunk: Data of single chunk.
    :param column_names: Names of feature columns.
    :param num_frames: Number of frames in the whole movie.
    :return: List with all features for single chunk.
    """
    frame_numbers_all = [frame_num + 1 for frame_num in range(num_frames)]
    frame_num_chunk = np.unique(chunk['frame_num'])
    print(len(set(frame_numbers_all) - set(frame_num_chunk)))
    print(len(frame_num_chunk))
    print(len(frame_numbers_all))

    # Creating DataFrame with frames where no objects were detected
    difference_nums = set(frame_numbers_all) - set(frame_num_chunk)
    difference_list = [[diff_frame_num, 0, 0, 0, 0, 0, 0] for diff_frame_num in difference_nums]
    difference_df = pd.DataFrame(difference_list, columns=column_names)
    print(difference_df)

    # exit()
    # =================
    num_people = get_num_people(chunk)

    print(1)
    # print(num_people)
    # print(len(num_people))
    num_non_people = get_num_non_people(chunk)
    print(2)
    # print(num_non_people)
    # print(len(num_non_people))
    num_all_objects = get_num_all_objects(num_people, num_non_people)
    print(3)
    # print(num_all_objects)
    # print(len(num_all_objects))
    total_people_area = get_total_people_area(chunk)
    print(4)
    # print(total_people_area)
    # print(len(total_people_area))
    total_non_people_area = get_total_non_people_area(chunk)
    print(5)
    # print(total_non_people_area)
    # print(len(total_non_people_area))
    total_detected_area = get_total_detected_area(total_people_area, total_non_people_area)
    print(6)
    # print(total_detected_area)
    # print(len(total_detected_area))
    chunk_features_list = [frame_num_chunk, num_people, num_non_people, num_all_objects,
                           total_people_area, total_non_people_area, total_detected_area]

    chunk_features = pd.DataFrame(columns=column_names)
    for feature_i, feature_name in enumerate(column_names):
        chunk_features[feature_name] = chunk_features_list[feature_i]

    # start_time = time()
    # print(chunk_features)
    # for frame_num in frame_numbers_all:
    #     if frame_num not in chunk_features['frame_num']:
    #         print(frame_num)
    #         empty_row = {column_names[i + 1]: 0 for i in range(len(column_names) - 1)}
    #         empty_row['frame_num'] = frame_num
    #         chunk_features = chunk_features.append([empty_row], ignore_index=True)
    # print(time() - start_time)
    # chunk_features = chunk_features.sort_values(by='frame_num')
    # print(chunk_features)

    # exit()

    result_features = chunk_features.append(difference_df, ignore_index=True)
    result_features['frame_num'] = pd.to_numeric(result_features['frame_num'])
    result_features = result_features.sort_values(by='frame_num')

    return result_features


def generate_all_features(path_to_lumi, path_to_old_features):
    """
    Generates all needed object detection features
    and saves `.csv` file with all generated features per each frame.
    :param path_to_lumi: Path to .csv file with Luminoth results for each frame.
    :param path_to_old_features: Path to `.csv` with old features containing total number of movie frames.
    """
    # Creating empty DataFrame with all given columns
    result_columns = ['frame_num', 'num_people', 'num_non_people', 'num_total_objects',
                      'area_people', 'area_non_people', 'area_total_objects']
    # features_df = pd.DataFrame(columns=result_columns)
    print()

    df = pd.read_csv(path_to_lumi)
    old_features = pd.read_csv(path_to_old_features, skiprows=1)
    # print(df)
    # print(old_features)
    # print(old_features.columns)
    # print(len(old_features['Frame Number']))
    number_of_all_frames = len(old_features['Frame Number'])

    all_features = process_chunk(df, result_columns, number_of_all_frames)

    print(all_features)

    # for chunk in pd.read_csv(path_to_lumi, chunksize=chunksize):
    #     chunk_start_time = time()
    #     all_chunks_len.append(len(chunk))
    #     # print(chunk.columns)
    #     # print(chunk.head())
    #     print(chunk.shape)
    #     chunk = chunk.drop_duplicates(chunk.columns, keep='first')
    #     # print(chunk.columns)
    #     # print(chunk.head())
    #
    #     # print(chunk.columns)
    #     print(chunk.shape)
    #     # print(np.unique(chunk['label']))
    #
    #     chunk_features = process_chunk(chunk, result_columns)
    #     # features_df.concat([chunk_features], ignore_index=True, inplace=True)
    #     features_df = features_df.append(chunk_features)
    #     # print(features_df.shape)
    #     # print(features_df.columns)
    #     print('{}/{}   Time: {} sec'.format(sum(all_chunks_len), number_of_rows,
    #                                         round(time() - chunk_start_time, 2)))
    #
    # # Saving final features
    # print(len(features_df))
    # features_df.drop_duplicates(inplace=True)
    # print(len(features_df))
    all_features.to_csv('requiem_lumi_features.csv', index=False)


def generate_all_features_with_chunks(path_to_lumi, chunksize):
    """
    Generates all needed object detection features
    and saves `.csv` file with all generated features per each frame.
    :param path_to_lumi: Path to .csv file with Luminoth results for each frame.
    :param chunksize: The size of single chunk.
    """
    number_of_rows = 610163700
    print('Number of chunks: {} ===== Chunk size: {}'.format(number_of_rows // chunksize, chunksize))

    # Creating empty DataFrame with all given columns
    result_columns = ['frame_num', 'num_people', 'num_non_people', 'num_total_objects',
                      'area_people', 'area_non_people', 'area_total_objects']
    features_df = pd.DataFrame(columns=result_columns)

    all_chunks_len = []

    for chunk in pd.read_csv(path_to_lumi, chunksize=chunksize):
        chunk_start_time = time()
        all_chunks_len.append(len(chunk))
        # print(chunk.columns)
        # print(chunk.head())
        print(chunk.shape)
        chunk = chunk.drop_duplicates(chunk.columns, keep='first')
        # print(chunk.columns)
        # print(chunk.head())

        # print(chunk.columns)
        print(chunk.shape)
        # print(np.unique(chunk['label']))

        chunk_features = process_chunk(chunk, result_columns)
        # features_df.concat([chunk_features], ignore_index=True, inplace=True)
        features_df = features_df.append(chunk_features)
        # print(features_df.shape)
        # print(features_df.columns)
        print('{}/{}   Time: {} sec'.format(sum(all_chunks_len), number_of_rows,
                                            round(time() - chunk_start_time, 2)))

    # Saving final features
    print(len(features_df))
    features_df.drop_duplicates(inplace=True)
    print(len(features_df))
    # features_df.to_csv('lumi_features_test.csv', index=False)


if __name__ == '__main__':
    CHUNKSIZE = 10 ** 6
    PATH_TO_LUMI_RESULT = 'lumi_results.csv'
    PATH_TO_OLD_FEATURES = '../movies/Requiem/Requiem.stats.csv'
    generate_all_features(PATH_TO_LUMI_RESULT, PATH_TO_OLD_FEATURES)
    # generate_all_features_with_chunks(PATH_TO_LUMI_RESULT, CHUNKSIZE)
