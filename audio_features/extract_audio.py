import os
from moviepy.editor import VideoFileClip
from pydub import AudioSegment


def save_audio(audio_clip, audio_filename):
    """
    Saves audio in `.mp3` or `.wav` format.
    :param audio_clip: AudioFileClip object.
    :param audio_filename: Resulting audio filename.
    """
    audio_clip.write_audiofile(filename=os.path.join('extracted_audio', audio_filename))


def extract_audio_clip(path_to_video):
    """
    Extracts audio clip from video.
    :param path_to_video: Path to input video.
    :return: Extracted audio clip in AudioFileClip format.
    """
    video_clip = VideoFileClip(path_to_video)
    audio_clip = video_clip.audio
    return audio_clip


def convert_to_mono(path_to_file):
    """
    Converts `.mp3` or `.wav` from stereo to mono and saves it .
    :param path_to_file: Path to input audio file.
    """
    if path_to_file.endswith('.wav'):
        sound = AudioSegment.from_wav(path_to_file)
        sound = sound.set_channels(1)
        sound.export(path_to_file, format="wav")

    elif path_to_file.endswith('.mp3'):
        sound = AudioSegment.from_mp3(path_to_file)
        sound = sound.set_channels(1)
        sound.export(path_to_file, format="mp3")

    else:
        pass


if __name__ == '__main__':
    # PATH_TO_VIDEO = '../PySceneDetect/Zero/scenes/Zero_scene_29.mp4'
    # PATH_TO_RESULT_AUDIO = 'extracted_audio_test_zero.wav'
    PATH_TO_VIDEO = '../movies/Requiem.mkv'
    PATH_TO_RESULT_AUDIO = 'requiem_audio.mp3'
    save_audio(extract_audio_clip(PATH_TO_VIDEO), PATH_TO_RESULT_AUDIO)
    convert_to_mono(os.path.join('extracted_audio', PATH_TO_RESULT_AUDIO))
