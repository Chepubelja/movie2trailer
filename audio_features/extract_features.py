import os

import numpy as np
from pyAudioAnalysis import audioBasicIO
from pyAudioAnalysis.audioFeatureExtraction import stFeatureExtraction, mtFeatureExtraction


def read_audio(audio_filename):
    """
    Reads audio file and returns frame rate and signal.
    :param audio_filename: Input audio file.
    :return: Signal, frame rate.
    """
    audio_directory = os.path.join('extracted_audio', audio_filename)
    frame_rate, signal = audioBasicIO.readAudioFile(audio_directory)
    print(signal)
    print(len(signal))
    print(frame_rate)
    return signal, frame_rate


def extract_short_term_features(signal, frame_rate):
    """
    Extracts short-term audio features.
    :param signal: Input signal.
    :param frame_rate: Frame rate of audio signal.
    :return: Short-term audio features (a numpy array (n_feats x numOfShortTermWindows))
        and corresponding feature names.
    """
    audio_features, feature_names = stFeatureExtraction(signal, frame_rate,
                                                        0.050 * frame_rate, 0.050 * frame_rate)
    print(audio_features)
    print(feature_names)
    print('===================================')
    print(audio_features.shape)
    print(len(feature_names))
    return audio_features, feature_names


def extract_mid_term_features(signal, frame_rate):
    """
    Extracts mid-term audio features.
    :param signal: Input signal.
    :param frame_rate: Frame rate of audio signal.
    :return: Mid-term audio features and corresponding feature names.
    """
    mid_audio_features, _, mid_feature_names = mtFeatureExtraction(signal, frame_rate,
                                                                   mt_step=frame_rate,
                                                                   mt_win=frame_rate,
                                                                   st_step=0.050 * frame_rate,
                                                                   st_win=0.050 * frame_rate)
    print(mid_audio_features)
    print(mid_feature_names)
    print('===================================')
    print(mid_audio_features.shape)
    print(len(mid_feature_names))
    return mid_audio_features, mid_feature_names


def save_all_features(audio_filename, path_to_folder, short_features, short_names, mid_features, mid_names):
    """
    Saves both short-term and mid-term audio features into 2 separate `.csv` files.
    :param audio_filename: Input audio filename.
    :param path_to_folder: Resulting folder path.
    :param short_features: NumPy array with short-term audio features.
    :param short_names: List with short-term audio features names.
    :param mid_features: NumPy array with mid-term audio features.
    :param mid_names: List with mid-term audio features names.
    """
    if not os.path.exists(path_to_folder):
        os.makedirs(path_to_folder)

    # Saving short-term features
    np.savetxt("{}_short_features.csv".format(os.path.join(path_to_folder, audio_filename.split('.')[0])),
               short_features.T, delimiter=",", header=','.join(short_names), comments='')

    # Saving mid-term features
    np.savetxt("{}_mid_features.csv".format(os.path.join(path_to_folder, audio_filename.split('.')[0])),
               mid_features.T, delimiter=",", header=','.join(mid_names), comments='')


# plt.subplot(2,1,1); plt.plot(F[0,:]); plt.xlabel('Frame no'); plt.ylabel(f_names[0])
# plt.subplot(2,1,2); plt.plot(F[1,:]); plt.xlabel('Frame no'); plt.ylabel(f_names[1])
# plt.show()


if __name__ == '__main__':
    # AUDIO_FILENAME = 'extracted_audio_test_zero.wav'
    AUDIO_FILENAME = 'requiem_audio.mp3'
    PATH_TO_RESULT_FOLDER = 'extracted_features'

    SIGNAL, FRAME_RATE = read_audio(audio_filename=AUDIO_FILENAME)
    SHORT_AUDIO_FEATURES, SHORT_FEATURE_NAMES = extract_short_term_features(signal=SIGNAL, frame_rate=FRAME_RATE)
    MID_AUDIO_FEATURES, MID_FEATURE_NAMES = extract_mid_term_features(signal=SIGNAL, frame_rate=FRAME_RATE)

    save_all_features(AUDIO_FILENAME, PATH_TO_RESULT_FOLDER,
                      SHORT_AUDIO_FEATURES, SHORT_FEATURE_NAMES,
                      MID_AUDIO_FEATURES, MID_FEATURE_NAMES)
