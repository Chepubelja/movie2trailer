"""
Module for merging visual features together into single `.csv`.
"""

import os
import pandas as pd


def merge(feature_filenames: list, result_filename: str):
    """
    Merges multiple frame-by-frame visual features to one `.csv`.
    :param feature_filenames: List with all visual features `.csv` file names.
    :param result_filename: Result `.csv` filename to write merged features into.
    """
    feature_dfs = []
    for file_i, filename in enumerate(feature_filenames):
        if file_i == 0:
            feature_dfs.append(pd.read_csv(filename, skiprows=1))
        else:
            feature_dfs.append(pd.read_csv(filename))

    # print(feature_dfs)

    merged_features = pd.concat(feature_dfs, axis=1)

    print(merged_features)
    print(merged_features.columns)

    print((merged_features['Frame Number'].all() == merged_features['frame_num'].all()))

    del merged_features['Frame Number']
    # print((s1.isnull() == s2.isnull()).all() and (s1[s1.notnull()] == s2[s2.notnull()]).all())

    if not os.path.exists('visual_features'):
        os.makedirs('visual_features')

    merged_features.to_csv(os.path.join('visual_features', result_filename), index=False)


if __name__ == '__main__':
    COLOR_FEATURES_FILENAME = 'movies/Requiem/Requiem.stats.csv'
    LUMI_FEATURES_FILENAME = 'lumi_detection/requiem_lumi_features.csv'

    FEATURES_FILENAMES = [COLOR_FEATURES_FILENAME, LUMI_FEATURES_FILENAME]

    RESULT_FILENAME = 'visual_features.csv'
    merge(FEATURES_FILENAMES, RESULT_FILENAME)
